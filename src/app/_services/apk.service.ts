import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { ApkModel } from '@app/_models/apkModel';

@Injectable({ providedIn: 'root' })
export class ApkService {

    constructor(
        private router: Router,
        private http: HttpClient
    ) { }

    postDetails(data: ApkModel) {
        return this.http.post(`${environment.apiUrl}/installApk`, data.id);
    }
}