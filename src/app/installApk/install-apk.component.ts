import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApkModel } from '@app/_models/apkModel';

import { ApkService } from '@app/_services/apk.service'
import { map } from 'rxjs/operators';

@Component({ templateUrl: 'install-apk.component.html' })
export class InstallAPKComponent implements OnInit {
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private apkService: ApkService
    ) { }

    ngOnInit() {
    }

    apkData: ApkModel = new ApkModel();

    //array varibales to store csv data
    lines = []; //for headings
    linesR = []; // for rows
    //File upload function
    changeListener(files: FileList) {
        console.log(files);
        if (files && files.length > 0) {
            let file: File = files.item(0);
            console.log(file.name);
            console.log(file.size);
            console.log(file.type);
            //File reader method
            let reader: FileReader = new FileReader();
            reader.readAsText(file);
            reader.onload = (e) => {
                let csv: any = reader.result;
                let allTextLines = [];
                allTextLines = csv.split(/\r|\n|\r/);

                //Table Headings
                let headers = allTextLines[0].split(';');
                let data = headers;
                let tarr = [];
                for (let j = 0; j < headers.length; j++) {
                    tarr.push(data[j]);
                }
                //Pusd headings to array variable
                this.lines.push(tarr);


                // Table Rows
                let tarrR = [];

                let arrl = allTextLines.length;
                let rows = [];
                for (let i = 1; i < arrl; i++) {
                    rows.push(allTextLines[i].split(';'));

                }

                for (let j = 0; j < arrl; j++) {

                    tarrR.push(rows[j]);

                }
                //Push rows to array variable
                this.linesR.push(tarrR);
            }
        }
    }

    installApk() {
        this.linesR.forEach(element => {
            this.apkData.id = this.linesR[0];
            this.apkData.name = this.linesR[1];
        });
        var data = this.apkService.postDetails(this.apkData);
    }

    unInstallApk() {
        this.linesR.forEach(element => {
            this.apkData.id = this.linesR[0];
            this.apkData.name = this.linesR[1];
        });
        var data = this.apkService.postDetails(this.apkData);
    }
}