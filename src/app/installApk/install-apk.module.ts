import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { InstallAPKRoutingModule } from './install-apk-routing.module';
import { InstallAPKComponent } from './install-apk.component';
import { LayoutComponent } from './layout.component';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        InstallAPKRoutingModule
    ],
    declarations: [
        LayoutComponent,
        InstallAPKComponent
    ]
})
export class InstallAPKModule { }