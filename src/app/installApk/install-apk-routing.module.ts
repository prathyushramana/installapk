import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InstallAPKComponent } from './install-apk.component';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: '', component: InstallAPKComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class InstallAPKRoutingModule { }