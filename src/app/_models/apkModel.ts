export class ApkModel {
    id: string;
    name: string;

    constructor(init?: Partial<ApkModel>) {
        Object.assign(this, init);
    }
}
